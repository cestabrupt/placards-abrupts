# ~/ABRÜPT/PLACARDS ABRUPTS/*

Le [placard abrupt](https://abrupt.ch/antilivre#placard) : L’idée de l’antilivre s’affiche, fomente des ruptures numériques parmi les langueurs modernes. Le placard fouille son histoire politique pour transformer son essence textuelle en une rencontre graphique. Une feuille A3, son recto qui tonne sa dialectique, puis son verso qui grave sa représentation, pour que le placard orne le réel, y fasse brèche, laisse poindre les possibles des valeurs renversées.

La [liste](https://abrupt.ch/cyberpoetique/tags/placard/) de nos *placards abrupts* est publiée dans notre [Cyberpoétique](https://abrupt.ch/cyberpoetique/).


## Dans le domaine public volontaire

Tous les placards abrupts sont dédiés au domaine public.

Ils sont mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d’informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
